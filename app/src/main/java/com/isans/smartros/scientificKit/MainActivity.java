package com.isans.smartros.scientificKit;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Toast;
import org.ros.android.smartRosActivity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class MainActivity extends smartRosActivity {

    private String _busyId;
    private int _fps;
    private Context mainContext=this;
    private int mBaudrate=115200;	//set the default baud rate to 115200
    private String mPassword="AT+PASSWOR=DFRobot\r\n";
    private String mBaudrateBuffer = "AT+CURRUART="+mBaudrate+"\r\n";
    private static BluetoothGattCharacteristic mSCharacteristic, mModelNumberCharacteristic, mSerialPortCharacteristic, mCommandCharacteristic;
    BluetoothLeService mBluetoothLeService;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private LeDeviceListAdapter mLeDeviceListAdapter=null;
    private BluetoothAdapter mBluetoothAdapter;
    private boolean mScanning =false;
    private String mDeviceName;
    private String mDeviceAddress;
    public enum connectionStateEnum{isNull, isScanning, isToScan, isConnecting , isConnected, isDisconnecting};
    public connectionStateEnum mConnectionState = connectionStateEnum.isNull;
    private static final int REQUEST_ENABLE_BT = 1;
    private Handler mHandler= new Handler();
    public boolean mConnected = false;
    private final static String TAG = MainActivity.class.getSimpleName();
    public static final String SerialPortUUID="0000dfb1-0000-1000-8000-00805f9b34fb";
    public static final String CommandUUID="0000dfb2-0000-1000-8000-00805f9b34fb";
    public static final String ModelNumberStringUUID="00002a24-0000-1000-8000-00805f9b34fb";
    private Timer mTimer = null;

    private long startTime = 0;
    private long endTime;
    private long serialStartTime = 0;

    // added by bcc
    private String sensor_data = ""; // for parsing received data

    public void serialSend(String theString){
        if (mConnectionState == connectionStateEnum.isConnected) {
            mSCharacteristic.setValue(theString);
            mBluetoothLeService.writeCharacteristic(mSCharacteristic);
        }
    }

    public void serialBegin(int baud){
        mBaudrate=baud;
        mBaudrateBuffer = "AT+CURRUART="+mBaudrate+"\r\n";
    }

    private Runnable mConnectingOverTimeRunnable = new Runnable(){

        @Override
        public void run() {
            if(mConnectionState==connectionStateEnum.isConnecting)
                mConnectionState=connectionStateEnum.isToScan;
            mBluetoothLeService.close();
        }};

    private Runnable mDisonnectingOverTimeRunnable = new Runnable(){

        @Override
        public void run() {
            if(mConnectionState==connectionStateEnum.isDisconnecting)
                mConnectionState=connectionStateEnum.isToScan;
            mBluetoothLeService.close();
        }};

    public MainActivity(){
        super("scientificKit Node");
    }
    private class MyTask extends TimerTask{

        @Override
        public void run() {
            int size = mLeDeviceListAdapter.getCount();
            Log.d(TAG, "size: "+size);
            for(int which = 0; which < size; which++) {
                final BluetoothDevice device = mLeDeviceListAdapter.getDevice(which);
                Log.d(TAG, "address: " + device.getAddress());
                if (!mDeviceAddress.equals(device.getAddress())) continue;
                scanLeDevice(false);
                if (mBluetoothLeService.connect(mDeviceAddress)) {
                    Log.d(TAG, "Connect request success");
                    mConnectionState=connectionStateEnum.isConnecting;
                    mHandler.postDelayed(mConnectingOverTimeRunnable, 10000);
                    mTimer.cancel();
                }
                else {
                    Log.d(TAG, "Connect request fail");
                    mConnectionState=connectionStateEnum.isToScan;
                }
            }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        moveTaskToBack(true);   // turn background task

        /// Bluno
        onCreateProcess();
        serialBegin(115200);
        scanLeDevice(true);
        endTime = System.currentTimeMillis();
    }   // onCreate
    private boolean boundary(String[] range, int idx){
        return (range.length > idx );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onStopProcess();
        onPauseProcess();
        onDestroyProcess();
    }

    @Override
    protected void onNewMessage(String s) {
        long endTime_ = System.currentTimeMillis();
        long duration;
        String[] parts = s.split("/");
        Log.d(TAG, s);
        if (parts.length < 2) return;

        String cmd = parts[1];
        if (cmd.equals("start_all")) {

        } else if (cmd.equals("reset_all")) {

        } else if (cmd.equals("fps")) {
            if (boundary(parts, 2)) {
                _fps = Integer.parseInt(parts[2]);
            }
        } else if (cmd.equals("sci_set_address")) {
            if (boundary(parts, 3)) {
                _busyId = parts[2];
                mDeviceAddress = parts[3];

                if (mTimer != null) {
                    mTimer.cancel();
                }
                if (mConnectionState == connectionStateEnum.isConnected) {
                    mBluetoothLeService.disconnect();
                    Log.d(TAG, "disconnected");
                    mHandler.postDelayed(mDisonnectingOverTimeRunnable, 10000);
                    mConnectionState = connectionStateEnum.isDisconnecting;
                }
                mTimer = new Timer();
                MyTask task = new MyTask();
                mTimer.schedule(task, 1000, 2000);
            }
        } else if (cmd.equals("sci_go_forward") || cmd.equals("sci_go_backward")) {
            if (boundary(parts, 3)) {
                String port = parts[2];
                int value = 0;
                switch (parts[3]) {
                    case "stop":
                        value = 0;
                        break;
                    case "slow":
                        value = 50;
                        break;
                    case "medium":
                        value = 100;
                        break;
                    case "fast":
                        value = 255;
                        break;
                }
                serialSend(cmd + '/' + port + '/' + value + '\n');
            }
        } else if (cmd.equals("sci_stop_wheel") || cmd.equals("sci_stop_request")) {
            duration = (endTime_ - serialStartTime);
            if (duration > 300) {
                serialSend(cmd + '/' + '\n');
                serialStartTime = System.currentTimeMillis();
            }

        } else if (cmd.equals("sci_output_sensor")) {
            if (boundary(parts, 2)) {
                String value = parts[2];
                int onoff = 0;
                if(value.equals("High")){
                    onoff = 1;
                }
                else if (value.equals("Low")) {
                    onoff = 0;
                }
                serialSend(cmd + '/' + onoff + '\n');
            }
        } else if(cmd.equals("sci_request_analog") || cmd.equals("sci_request_digital")){
            if (boundary(parts, 2)) {
                String port = parts[2];
                serialSend(cmd + '/' + port + '\n');
            }
        }
    }

    protected void onResume(){
        super.onResume();
        Log.i(TAG, "BlUNOActivity onResume");
        onResumeProcess(); //onResume Process by BlunoLibrary
    }
    public void onCreateProcess() {
        if(!initiate()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(mainContext, R.string.error_bluetooth_not_supported,
                            Toast.LENGTH_SHORT).show();
                    ((Activity) mainContext).finish();
                }
            });
        }

        Intent gattServiceIntent = new Intent(mainContext, BluetoothLeService.class);
        mainContext.bindService(gattServiceIntent, mServiceConnection, Context.BIND_AUTO_CREATE);

        // Initializes list view adapter.
        mLeDeviceListAdapter = new LeDeviceListAdapter();

    }

    public void onResumeProcess() {
        System.out.println("BlUNOActivity onResume");
        // Ensures Bluetooth is enabled on the device. If Bluetooth is not
        // currently enabled,
        // fire an intent to display a dialog asking the user to grant
        // permission to enable it.
        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }
        mainContext.registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());

    }

    public void onPauseProcess() {
        System.out.println("BLUNOActivity onPause");
        scanLeDevice(false);
        mainContext.unregisterReceiver(mGattUpdateReceiver);
        mLeDeviceListAdapter.clear();
        mConnectionState=connectionStateEnum.isToScan;
        if(mBluetoothLeService!=null)
        {
            mBluetoothLeService.disconnect();
            mHandler.postDelayed(mDisonnectingOverTimeRunnable, 10000);
        }
        mSCharacteristic=null;

    }

    public void onStopProcess() {
        if(mBluetoothLeService!=null) {
            mHandler.removeCallbacks(mDisonnectingOverTimeRunnable);
            mBluetoothLeService.close();
        }
        mSCharacteristic=null;
    }

    public void onDestroyProcess() {
        mainContext.unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }

    boolean initiate() {
        // Use this check to determine whether BLE is supported on the device.
        // Then you can
        // selectively disable BLE-related features.
        if (!mainContext.getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_BLUETOOTH_LE)) {
            return false;
        }

        // Initializes a Bluetooth adapter. For API level 18 and above, get a
        // reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) mainContext.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            return false;
        }
        return true;
    }

    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @SuppressLint("DefaultLocale")
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            System.out.println("mGattUpdateReceiver->onReceive->action="+action);
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                mHandler.removeCallbacks(mConnectingOverTimeRunnable);

            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                mConnectionState = connectionStateEnum.isToScan;
                mHandler.removeCallbacks(mDisonnectingOverTimeRunnable);
                mBluetoothLeService.close();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                for (BluetoothGattService gattService : mBluetoothLeService.getSupportedGattServices()) {
                    System.out.println("ACTION_GATT_SERVICES_DISCOVERED  "+
                            gattService.getUuid().toString());
                }
                getGattServices(mBluetoothLeService.getSupportedGattServices());
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                if(mSCharacteristic==mModelNumberCharacteristic)
                {
                    if (intent.getStringExtra(BluetoothLeService.EXTRA_DATA).toUpperCase().startsWith("DF BLUNO")) {
                        mBluetoothLeService.setCharacteristicNotification(mSCharacteristic, false);
                        mSCharacteristic=mCommandCharacteristic;
                        mSCharacteristic.setValue(mPassword);
                        mBluetoothLeService.writeCharacteristic(mSCharacteristic);
                        mSCharacteristic.setValue(mBaudrateBuffer);
                        mBluetoothLeService.writeCharacteristic(mSCharacteristic);
                        mSCharacteristic=mSerialPortCharacteristic;
                        mBluetoothLeService.setCharacteristicNotification(mSCharacteristic, true);
                        mConnectionState = connectionStateEnum.isConnected;
                        sendRespMessage(String.format("_busy %s\n", _busyId));
                    }
                    else {
                        Toast.makeText(mainContext, "Please select DFRobot devices",Toast.LENGTH_SHORT).show();
                        mConnectionState = connectionStateEnum.isToScan;
                    }
                }
                else if (mSCharacteristic==mSerialPortCharacteristic) {
                    onSerialReceived(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
                    //sendRespMessage(String.format("_busy %s\n", _busyId));
                }
                System.out.println("displayData "+intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
            }
        }
    };

    void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            if(mLeDeviceListAdapter != null) {
                mLeDeviceListAdapter.clear();
                mLeDeviceListAdapter.notifyDataSetChanged();
            }
            if(!mScanning) {
                mScanning = true;
                mBluetoothAdapter.startLeScan(mLeScanCallback);
            }
        } else {
            if(mScanning) {
                mScanning = false;
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
            }
        }
    }

    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            System.out.println("mServiceConnection onServiceConnected");
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                ((Activity) mainContext).finish();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            System.out.println("mServiceConnection onServiceDisconnected");
            mBluetoothLeService = null;
        }
    };

    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(final BluetoothDevice device, int rssi,
                             byte[] scanRecord) {
            ((Activity) mainContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    System.out.println("mLeScanCallback onLeScan run ");
                    mLeDeviceListAdapter.addDevice(device);
                    mLeDeviceListAdapter.notifyDataSetChanged();
                }
            });
        }
    };

    private void getGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        String uuid = null;
        mModelNumberCharacteristic=null;
        mSerialPortCharacteristic=null;
        mCommandCharacteristic=null;
        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            uuid = gattService.getUuid().toString();
            System.out.println("displayGattServices + uuid="+uuid);

            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas =
                    new ArrayList<BluetoothGattCharacteristic>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
                uuid = gattCharacteristic.getUuid().toString();
                if(uuid.equals(ModelNumberStringUUID)){
                    mModelNumberCharacteristic=gattCharacteristic;
                    System.out.println("mModelNumberCharacteristic  "+mModelNumberCharacteristic.getUuid().toString());
                }
                else if(uuid.equals(SerialPortUUID)){
                    mSerialPortCharacteristic = gattCharacteristic;
                    System.out.println("mSerialPortCharacteristic  "+mSerialPortCharacteristic.getUuid().toString());
                }
                else if(uuid.equals(CommandUUID)){
                    mCommandCharacteristic = gattCharacteristic;
                    System.out.println("mSerialPortCharacteristic  "+mSerialPortCharacteristic.getUuid().toString());
                }
            }
            mGattCharacteristics.add(charas);
        }

        if (mModelNumberCharacteristic==null || mSerialPortCharacteristic==null || mCommandCharacteristic==null) {
            Toast.makeText(mainContext, "Please select DFRobot devices",Toast.LENGTH_SHORT).show();
            mConnectionState = connectionStateEnum.isToScan;
        }
        else {
            mSCharacteristic=mModelNumberCharacteristic;
            mBluetoothLeService.setCharacteristicNotification(mSCharacteristic, true);
            mBluetoothLeService.readCharacteristic(mSCharacteristic);
        }

    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

    private class LeDeviceListAdapter extends BaseAdapter {
        private ArrayList<BluetoothDevice> mLeDevices;
        private LayoutInflater mInflator;

        public LeDeviceListAdapter() {
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
            mInflator =  ((Activity) mainContext).getLayoutInflater();
        }

        public void addDevice(BluetoothDevice device) {
            if (!mLeDevices.contains(device)) {
                mLeDevices.add(device);
            }
        }

        public BluetoothDevice getDevice(int position) {
            return mLeDevices.get(position);
        }

        public void clear() {
            mLeDevices.clear();
        }

        @Override
        public int getCount() {
            return mLeDevices.size();
        }

        @Override
        public Object getItem(int i) {
            return mLeDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            return null;
        }
    }
    public void onSerialReceived(String theString) {
        //Once connection data received, this function will be called
        sensor_data += theString;
        if(sensor_data.substring(0,1).equals("<") &&
                sensor_data.substring(sensor_data.length()-1, sensor_data.length()).equals(">")){

            // packaged
            String cmds[] = sensor_data.split("/");
            if(cmds.length > 0 )
                cmds[0] = cmds[0].substring(1, cmds[0].length());
            sensor_data = "";
            StringBuilder recordValue = new StringBuilder();
            for(int i = 0; i < cmds.length; i++){
                if (cmds[i].equals(">")) break;
                recordValue.append(String.format("sci_response/%s %s\n", cmds[i], cmds[i+1]));
            }

            endTime = System.currentTimeMillis();
            long duration = (endTime - startTime);

            if(duration >= (1000/_fps)){
                sendRespMessage(recordValue.toString());
                startTime = System.currentTimeMillis();
            }
        }

        //The Serial data from the BLUNO may be sub-packaged, so using a buffer to hold the String is a good choice.
    }
}
